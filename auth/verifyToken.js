//verifyToken.js
var jwt = require('jsonwebtoken');
var config = require('../config');

var verifyToken = function (req, res, next) {
	var token = req.headers['x-access-token'];
	if(!token)
		res.status(403).send({auth:false, msg:"No token provided"});
	jwt.verify(token, config.secret, function(err, decoded){
		if(err)
			res.status(500).send({auth:false, msg: "Faild to authenticate token"});

		//save to request for use in other route
		req.userId = decoded.id;
		next();
	})
}

module.exports = verifyToken;