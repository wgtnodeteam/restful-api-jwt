// UserController.js
var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
router.use(bodyParser.urlencoded({extended:true}));
router.use(bodyParser.json());
var User = require('./user');
var verifyToken = require('./auth/verifyToken');

// Create a new user
router.post('/', function (req, res) {
	User.create({
		name: req.body.name,
		email: req.body.email,
		password: req.body.password
	}, function(err, user){
		if(err)
			return res.status(500).send("There was problem to adding information to the database");
		res.status(200).send(user);
	});
});

//Return all the users in the database
router.get('/', verifyToken, function(req, res){
	User.find({}, {password:0}, function(err, users){
		if(err)
			res.status(500).send("There was problem finding users");
		res.status(200).send(users);
	})
});

//Get a single user
router.get('/:id', verifyToken, function(req, res){
	User.findById(req.params.id, {password:0}, function(err, user){
		if(err)
			return res.status(500).send("There was a problem to fine the user");
		res.status(200).send(user);
	});
});

//Delete a user
router.delete('/:id', verifyToken, function(req, res){
	User.findByIdAndRemove(req.params.id, function(err, user){
		if(err)
			res.status(500).send("There was a problem to deleting the user");
		res.status(200).send("User "+user.name+" was deleted");
	})
});

//Update the single user in the database
router.put('/:id', verifyToken, function(req, res){
	User.findByIdAndUpdate(req.params.id, req.body, {new:true}, function(err, user){
		if(err)
			return res.status(500).send("There was problem to update the user")
		res.status(200).send(user);
	})
})

module.exports = router;