//app.js
var express = require('express');
var app = express();
var db = require('./db');

var TestController = require('./testController');
var UserController = require('./userController');
var ProductController = require('./controller/product/productController');
var OrderController = require('./controller/order/orderController');
var AuthController = require('./auth/authController');

// Main middleware
// app.use(function(err, req, res, next) {
//   // Do logging and user-friendly error message display
//   console.error(err);
//   res.status(500).send({status:500, message: 'internal error', type:'internal'}); 
// })

app.use('/test', TestController);
app.use('/users', UserController);
app.use('/product', ProductController);
app.use('/order', OrderController);
app.use('/api/auth', AuthController);

module.exports=app;