// UserController.js
var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
router.use(bodyParser.urlencoded({extended:true}));
router.use(bodyParser.json());

// Create a new user
var fs = require('fs');

//Sample data
var user = {
	"user4" : {
		"name" : "Arup",
		"password" : "123456",
		"profession" : "Developer",
		"id" : 4
	}
}

router.get('/', function(req, res){
	fs.readFile(__dirname + '/'+ 'users.json', 'utf8', function(err, data){
		console.log(data);
		res.end(data);
	})
})

router.post('/addUser', function(req, res){
	// First read existing users.
	fs.readFile(__dirname + '/'+ 'users.json', 'utf8', function(err, data){
		if(err)
			console.log(err);
		data = JSON.parse( data );
		data["user4"] = user["user4"];
		console.log(data);
		res.end( JSON.stringify(data) );
	});
})

router.get('/:id', function(req, res){
	fs.readFile(__dirname +'/'+ 'users.json', 'utf8', function(err, data){
		var users = JSON.parse(data);
		var user = users["user"+req.params.id];
		console.log(user);
		res.end( JSON.stringify(user));
	})
})

router.delete('/:id', function(req, res){
	fs.readFile(__dirname +'/'+ 'users.json', 'utf8', function(err, data){
		var users = JSON.parse( data );
		delete users["user"+req.params.id];
		res.end(JSON.stringify(users));
	})
})

module.exports = router;