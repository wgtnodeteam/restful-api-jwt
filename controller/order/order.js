//product.js
var mongoose = require('mongoose');
var orderSchema = new mongoose.Schema({
	user_id: { type: mongoose.Schema.ObjectId, required: true},
	product_id: { type: mongoose.Schema.ObjectId, required: true},
	qty: Number,
	price: Number
});

mongoose.model('Order', orderSchema)
module.exports = mongoose.model('Order');