//productController.js
var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
router.use(bodyParser.urlencoded({extended:true}))
router.use(bodyParser.json());
var Order = require('./order');
var verifyToken = require('../../auth/verifyToken');

router.get('/', verifyToken, function (req, res) {
	Order.find({}, function(err, order){
		if(err)
			res.status(500).send("There was problem to find any order");
		res.status(200).send(order);
	})
});

router.get('/:id', function(req, res){
	Order.findById(req.params.id, function(err, order){
		if(err)
			res.status(500).send("There was a problem to find order");
		res.status(200).send(order);
	})
});

router.post('/', function(req, res){
	Order.create({
		user_id:req.body.user_id,
		product_id:req.body.product_id,
		qty:req.body.qty,
		price:req.body.price
	},
	function(err, order){
		if(err)
			res.status(500).send("There was problem to add product to database");
		res.status(200).send(order);
	})
})

router.put('/:id', function(req, res){
	Order.findByIdAndUpdate(req.params.id, req.body, {new:true}, function(err, order){
		if(err)
			res.status(500).send("There was problem to update order");
		res.status(200).send(order);
	});
})

router.delete('/:id', function(req, res){
	Product.findByIdAndRemove(req.params.id, function(err, product){
		if(err)
			res.status(500).send("There was problem to delete product");
		res.status(200).send("Product : "+product.name+" was deleted");
	})
})

module.exports = router;