//productController.js
var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
router.use(bodyParser.urlencoded({extended:true}))
router.use(bodyParser.json());
var Product = require('./product');
var verifyToken = require('../../auth/verifyToken');

router.get('/', verifyToken, function (req, res) {
	Product.find({}, function(err, product){
		if(err)
			res.status(500).send("There was problem to find any product");
		res.status(200).send(product);
	})
});

router.get('/:id', function(req, res){
	Product.findById(req.params.id, function(err, product){
		if(err)
			res.status(500).send("There was a problem to find product");
		res.status(200).send(product);
	})
});

router.post('/', function(req, res){
	Product.create({
		name:req.body.name,
		sku:req.body.sku,
		price:req.body.price,
		description:req.body.description
	},
	function(err, product){
		if(err)
			res.status(500).send("There was problem to add product to database");
		res.status(200).send(product);
	})
})

router.put('/:id', function(req, res){
	Product.findByIdAndUpdate(req.params.id, req.body, {new:true}, function(err, product){
		if(err)
			res.status(500).send("There was problem to update product");
		res.status(200).send(product);
	});
})

router.delete('/:id', function(req, res){
	Product.findByIdAndRemove(req.params.id, function(err, product){
		if(err)
			res.status(500).send("There was problem to delete product");
		res.status(200).send("Product : "+product.name+" was deleted");
	})
})

module.exports = router;