//product.js
var mongoose = require('mongoose');
var productSchema = new mongoose.Schema({
	name: String,
	sku: Number,
	price: Number,
	description: String
});

mongoose.model('Product', productSchema)
module.exports = mongoose.model('Product');